Flink 的任务运行其实是采用多线程的方式，
这和 MapReduce 多 JVM 进程的 方式有很大的区别 
Fink 能够极大提高 CPU 使用效率，
在多个任务和 Task 之间通过 TaskSlot 方式共享系统资源，
每个 TaskManager 中通过管理多个 TaskSlot 
资源池进行对资源进行有 效管理。

下面针对 flink-conf.yaml 文件中的几个重要参数进行分析：
  jobmanager.heap.size：JobManager 节点可用的内存大小。
  taskmanager.heap.size：TaskManager 节点可用的内存大小。 
  taskmanager.numberOfTaskSlots：每台机器可用的 Slot 数量。 
  parallelism.default：默认情况下 Flink 任务的并行度。 
 上面参数中所说的 Slot 和 parallelism 的区别： 
  Slot 是静态的概念，是指 TaskManager 具有的并发执行能力。 
  parallelism 是动态的概念，是指程序运行时实际使用的并发能力。 
  设置合适的 parallelism 能提高运算效率。
 
 
 yarn-cluster
  -yn,--container <arg> 表示分配容器的数量，也就是 TaskManager 的数量。 
  -d,--detached：设置在后台运行。 
  -yjm,--jobManagerMemory<arg>:设置 JobManager 的内存，单位是 MB。 
  -ytm，--taskManagerMemory<arg>:设置每个 TaskManager 的内存，单位是 MB。 
  -ynm,--name:给当前 Flink application 在 Yarn 上指定名称。 
  -yq,--query：显示 yarn 中可用的资源（内存、cpu 核数） 
  -yqu,--queue<arg> :指定 yarn 资源队列 
  -ys,--slots<arg> :每个 TaskManager 使用的 Slot 数量。 
  -yz,--zookeeperNamespace<arg>:针对 HA 模式在 Zookeeper 上创建 NameSpace 
  -yid,--applicationID<yarnAppId> : 指定 Yarn 集群上的任务 ID,附着到一个后台独 立运行的 Yarn Session 中。
 
 
Flink中每一个worker(TaskManager)都是一个JVM进程，
它可能会在独立的线程（Solt） 上执行一个或多个 subtask。
Flink 的每个 TaskManager 为集群提供 Solt。
Solt 的数量通常 与每个 TaskManager 节点的可用 CPU 内核数成比例，
一般情况下 Slot 的数量就是每个节点 的 CPU 的核数。 
Slot 的 数 量 由 集 群 中 flink-conf.yaml 配 置 文 件 中 设 置 taskmanager.numberOfTaskSlots 的值为 3，
这个值的大小建议和节点 CPU 的数量保持一致。 


一个任务的并行度设置可以从 4 个层面指定: 
 Operator Level（算子层面）。 
 Execution Environment Level（执行环境层面）。 
 Client Level（客户端层面）。 
 System Level（系统层面）。 
这 些 并 行 度 的 优 先 级 为 
Operator Level>Execution Environment Level>Client Level>System Level。


//RedisCommand的命令 

flink 的时间语义
    事件时间 搭配Watermarks 一起使用 
    处理时间
    进入时间
    

flinkd的window 都是针对分组后的数据进行开窗计算
flink对于时间的处理：
    无法正确处理历史数据,
    无法正确处理超过最大无序边界的数据,
    结果将是不确定的
窗口应用函数：
    像批量处理，ProcessWindowFunction 会缓存 Iterable 和窗口内容，供接下来全量计算；
    或者像流处理，每一次有事件被分配到窗口时，都会调用 ReduceFunction 或者 AggregateFunction 来增量计算；
    或者结合两者，通过 ReduceFunction 或者 AggregateFunction 预聚合的增量计算结果在触发窗口时， 提供给 ProcessWindowFunction 做全量计算。

flink的时间窗口
    timeWindow 滚动窗口
     .timeWindow(Time.hours(1), Time.minutes(5))

状态后端
    默认情况下，State 会保存在 TaskManager 的内存中，
    CheckPoint 会存储在 JobManager 的内存中。
    State 和 CheckPoint 的存储位置取决于 StateBackend 的配置。
    Flink 一共提供 了 3 种 StateBackend 。 
    包 括 基 于 内 存 的 MemoryStateBackend 、 
    基 于 文 件 系 统 的 FsStateBackend，
    以及基于 RockDB 作为存储介质的 RocksDBState-Backend。
基于内存的状态管理具有非常快速和高效的特点，但也具有非常多的限制，
最主要的就 是内存的容量限制，一旦存储的状态数据过多就会导致系统内存溢出等问题，
从而影响整个 应用的正常运行。同时如果机器出现问题，整个主机内存中的状态数据都会丢失，
进而无法 恢复任务中的状态数据。因此从数据安全的角度建议用户尽可能地避免在生产环境中使用 
MemoryStateBackend。

和 MemoryStateBackend 有所不同，FsStateBackend 是基于文件系统的一种状态管理器，
 这里的文件系统可以是本地文件系统，也可以是 HDFS 分布式文件系统。FsStateBackend 
 更适合任务状态非常大的情况，例如应用中含有时间范围非常长的窗口计算
，或 Key/value State 状态数据量非常大的场景    

RocksDBStateBackend 采用异步的方式进行状态数据的 Snapshot，任务中的状态数据首 
先被写入本地 RockDB 中，这样在 RockDB 仅会存储正在进行计算的热数据，
而需要进行 CheckPoint 的时候，会把本地的数据直接复制到远端的 FileSystem 中。 
与 FsStateBackend 相比，RocksDBStateBackend 在性能上要比 FsStateBackend 高一些
主要是因为借助于 RocksDB 在本地存储了最新热数据，然后通过异步的方式再同步到文件系 统中，
但 RocksDBStateBackend 和 MemoryStateBackend 相比性能就会较弱一些。
RocksDB 克服了 State 受内存限制的缺点，同时又能够持久化到远端文件系统中，推荐在生产中使用。

还可以全局设置状态后端，在配置文件进行设置flink-conf.yaml
    state.backend: filesystem
    filesystem 表示使用 FsStateBackend, 
    jobmanager 表示使用 MemoryStateBackend 
    rocksdb 表示使用 RocksDBStateBackend。
    
 Flink 只保留最近成功生成的 1 个 CheckPoint，而当 Flink 程序失败时，
可以通过最近的 CheckPoint 来进行恢复

如果希望保留多个 CheckPoint，并能够根据实际需要选择其中一个进行恢复，
就会更加灵活。 添加如下配置，指定最多可以保存的 CheckPoint 的个数。 
state.checkpoints.num-retained: 2 

savepoint:
    Savepoints 是检查点的一种特殊实现，底层实现其实也是使用 Checkpoints 的机制。 
    Savepoints 是用户以手工命令的方式触发 Checkpoint,并将结果持久化到指定的存储路径中，
    其主要目的是帮助用户在升级和维护集群过程中保存系统中的状态数据，避免因为停机
    运维或者升级应用等正常终止应用的操作而导致系统无法恢复到原有的计算状态的情况，
    从而无法实现从端到端的 Excatly-Once 语义保证
    state.savepoints.dir: hdfs:/hadoop101:9000/savepoints

Window:
    flink 根据上游是不是keyed_stream，会有所不同
    对于keyed_stream 
        stream
               .keyBy(...)               <-  keyed versus non-keyed windows
               .window(...)              <-  required: "assigner"
              [.trigger(...)]            <-  optional: "trigger" (else default trigger)
              [.evictor(...)]            <-  optional: "evictor" (else no evictor)
              [.allowedLateness(...)]    <-  optional: "lateness" (else zero)
              [.sideOutputLateData(...)] <-  optional: "output tag" (else no side output for late data)
               .reduce/aggregate/apply()      <-  required: "function"
              [.getSideOutput(...)]      <-  optional: "output tag"
    对于非keyed_stream:
        stream
               .windowAll(...)           <-  required: "assigner"
              [.trigger(...)]            <-  optional: "trigger" (else default trigger)
              [.evictor(...)]            <-  optional: "evictor" (else no evictor)
              [.allowedLateness(...)]    <-  optional: "lateness" (else zero)
              [.sideOutputLateData(...)] <-  optional: "output tag" (else no side output for late data)
               .reduce/aggregate/apply()      <-  required: "function"
              [.getSideOutput(...)]      <-  optional: "output tag"
其中上面的[]中的操作是可选的

//flink window 
在定义了窗口分配器之后，我们需要指定我们想要在这些窗口中的每一个上执行的计算。
这是window function 的职责，它用于在系统确定窗口已准备好处理时处理每个（可能是键控的）窗口的元素（请参阅触发器以了解 Flink 如何确定窗口何时准备好）。
的窗函数可以是一个ReduceFunction，AggregateFunction或ProcessWindowFunction。
前两个可以更有效地执行（参见状态大小部分），因为 Flink 可以在每个窗口的元素到达时增量聚合它们。AProcessWindowFunction获取Iterable包含在窗口中的所有元素的an以及有关元素所属窗口的附加元信息。
带有 a 的窗口转换ProcessWindowFunction不能像其他情况一样有效地执行，因为 Flink在调用函数之前必须在内部缓冲窗口的所有元素。
这可以通过将 aProcessWindowFunction与 a组合来缓解ReduceFunction，或者AggregateFunction同时获取窗口元素的增量聚合和ProcessWindowFunction接收的附加窗口元数据 。我们将查看每个变体的示例。

  
当使用事件时间窗口时，元素可能会延迟到达，即Flink 用来跟踪事件时间进度的水印已经超过了元素所属窗口的结束时间戳。
默认情况下，当水印超过窗口末尾时，将删除后期元素。但是，Flink 允许为窗口操作符指定最大允许延迟。Allowed lateness 指定元素在被丢弃之前可以延迟多长时间，其默认值为 0。 在 watermark 已经通过窗口末尾之后但在它通过窗口末尾之前到达的元素加上允许的延迟，仍然添加到窗口中。根据使用的触发器，延迟但未丢弃的元素可能会导致窗口再次触发

 
当使用GlobalWindows窗口分配器时，没有数据被认为是延迟的，因为全局窗口的结束时间戳是Long.MAX_VALUE。

在使用窗口的时候需要注意的事项：
Windows 可以定义很长一段时间（例如几天、几周或几个月），因此会积累非常大的状态。
在估计加窗计算的存储要求时，需要记住几条规则：
Flink 为每个元素所属的每个窗口创建一个副本。
鉴于此，滚动窗口保留每个元素的一个副本（一个元素只属于一个窗口，除非它被延迟删除）。
相比之下，滑动窗口为每个元素创建多个，如窗口分配器部分所述。
因此，大小为 1 天和滑动 1 秒的滑动窗口可能不是一个好主意。
ReduceFunction并且AggregateFunction可以显着降低存储需求，
因为它们急切地聚合元素并且每个窗口只存储一个值。
相反，仅使用 aProcessWindowFunction需要累积所有元素。
使用 anEvictor可以防止任何预聚合，因为在应用计算之前，窗口的所有元素都必须通过evictor

本机启动reids:redis-server ./redis.conf

// flink time
flink的时间语义：
    默认是process time,除此之外还有event time 和  Ingestion time  
    对于数据，如果能使用事件时间，那么就需要使用事件时间，如果没有事件时间，那只能使用
    处理时间来替换
    对于flink的处理乱序时间，可以使用周期性water mark或间歇性水位线

    
    
    

