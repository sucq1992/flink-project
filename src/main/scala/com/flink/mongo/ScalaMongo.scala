package com.flink.mongo

import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase, SingleObservable}

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 22:21:40
  */
object ScalaMongo {
  def main(args: Array[String]): Unit = {
    val client = MongoClient("mongodb://localhost:27017")
    val database: MongoDatabase = client.getDatabase("my_mongo")
    val doc: MongoCollection[Document] = database.getCollection("account")
    val result: SingleObservable[Document] = doc.find().first()
    val data: String = result.toString
    println(data)
    client.close()

  }
}
