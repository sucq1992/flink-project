package com.flink.dstream

import com.flink.operator_level.myStationLogSource
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 16:42:31
  */
object FlinkTimeProcess {
  def main(args: Array[String]): Unit = {
    /**
      * flink的时间语义
      */
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    environment.setParallelism(2)
    // 设置时间语义为事件时间
    environment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    // 进入时间
    environment.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime)
    // 处理时间
    environment.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime)

    environment.addSource(new myStationLogSource)
        .print()
    environment.execute(this.getClass.getName)
  }
}
