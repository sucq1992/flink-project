package com.flink.dstream

import com.flink.operator_level.{StationLog, myStationLogSource}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 16:47:37
  */
/**
  * water mark 用来处理数据乱序的问题
  * 可以在后面设置允许数据迟到，添加侧输出流
  */

object FlinkWaterMarks {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    environment.setParallelism(6)
    val data: DataStream[StationLog] = environment.addSource(new myStationLogSource)
    //根据event time 有序的数据流
    data.assignAscendingTimestamps(_.callTime)
    environment.execute(this.getClass.getName)
  }
}
