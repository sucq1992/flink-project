package com.flink.dstream

import com.flink.operator_level.{StationLog, myStationLogSource}
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-09 09:32:50
  */
object FlinkWaterMarkAndEventTime {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    val stream: DataStream[StationLog] = environment.addSource(new myStationLogSource)
    //根据event time的指定有序的数据流
    stream.assignAscendingTimestamps(_.callTime)
    // 对于乱序数据流 引入water mark 和event time
    // 1. 周期性
           // 指定计算的延迟的数据最后的元素最大延迟时间
      stream.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[StationLog](Time.seconds(45)) {
        override def extractTimestamp(element: StationLog) = {
          // 设置event  time
          element.callTime
        }
      })

// 2.间断性
//      stream.assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks[StationLog]{
//        override def checkAndGetNextWatermark(lastElement: StationLog, extractedTimestamp: Long) = {
//          if (lastElement.sid.equals("station_1")){
//
//          }
//        }
//
//        override def extractTimestamp(element: StationLog, previousElementTimestamp: Long) = {
//
//        }
//      })


    environment.execute(this.getClass.getName)
  }
}


//通过单独的一个类完成延迟数据和event time的编写
class myEventProcess(delay:Long) extends AssignerWithPeriodicWatermarks[StationLog]{
  //设置最大的时间
  var maxTime :Long=0

  override def getCurrentWatermark: Watermark ={
    new Watermark(maxTime-delay)  // 创建水位线
  }


  override def extractTimestamp(element: StationLog, previousElementTimestamp: Long): Long = {
    maxTime=maxTime.max(element.callTime)
    element.callTime
  }
}

