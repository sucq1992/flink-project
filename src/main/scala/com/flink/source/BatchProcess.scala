package com.flink.source

import org.apache.flink.api.scala.ExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 22:53:18
  */
object BatchProcess {
  def main(args: Array[String]): Unit = {
    val environment: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    // 导入隐式转换，否则报错
    import org.apache.flink.api.scala._
    // UTF-8 默认是
    environment.readTextFile("data/word.txt","UTF-8")
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_,1))
      .groupBy(0)
      .sum(1)
      .print()


  }
}
