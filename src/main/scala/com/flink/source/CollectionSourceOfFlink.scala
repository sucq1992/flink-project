package com.flink.source

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 06:55:12
  */
object CollectionSourceOfFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
      // 根据元素
    //    environment.fromElements("hello","scala","hbase").print()


    environment.fromCollection(Array(
      new User("01","scala",23),
      new User("02","hive",24),
      new User("03","hbase",25),
      new User("04","spark",26),
      new User("05","flink",21)

    )).filter(_.uage>22)
        .print()

    environment.execute(this.getClass.getName)

  }
}

case class User(uid:String,uname:String,uage:Int)