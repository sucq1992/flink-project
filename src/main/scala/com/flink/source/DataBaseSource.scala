package com.flink.source

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}

import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 17:27:29
  */
object DataBaseSource {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.addSource(new myDBSource)
      .print()
    environment.execute(this.getClass.getName)

  }
}

/**
  * 使用富函数，带有生命周期，用来读取数据库或者写入数据库
  */
class myDBSource extends  RichSourceFunction[UserBean]{
  var conn:Connection =_
  var pst :PreparedStatement =_
  val username= "root"
  val password="scq110801502"
  val driverClass="com.mysql.jdbc.driver"
  val url="jdbc:mysql://localhost:3306/mydb?useUnicode=true&characterEncoding=utf-8&relaxAutoCommit=true" +
    "&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true"

  override def open(parameters: Configuration): Unit = {
      conn=DriverManager.getConnection(url,username,password)
    pst=conn.prepareStatement("select * from t_user")
  }

  override def run(ctx: SourceFunction.SourceContext[UserBean]): Unit = {
    val set: ResultSet = pst.executeQuery()
    while (set.next()){
      val bean = new UserBean(set.getInt("id"),
        set.getString("name"),
        set.getString("loc"))
      ctx.collect(bean)
    }

  }

  override def cancel(): Unit = {
    false
  }

  override def close(): Unit = {
    conn.close()
    pst.close()
  }
}

/**
  * flink 使用rich fucntion 读取数据库
  * @param id
  * @param name
  * @param loc
  */
case class UserBean(id:Int,name:String,loc:String)