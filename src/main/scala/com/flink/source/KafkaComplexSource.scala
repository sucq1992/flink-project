package com.flink.source

import java.util.Properties

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, KafkaDeserializationSchema}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 17:24:33
  */
object KafkaComplexSource {
  def main(args: Array[String]): Unit = {
    lazy val topic: String = "my_topic"
    lazy val flag = true

    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    val props = new Properties()
    props.setProperty("bootstrap.servers", "node1:9092,node2:9092,node3 3:9092")
    props.setProperty("group.id", "fink01")
    props.setProperty("key.deserializer", classOf[StringDeserializer].getName)
    props.setProperty("value.deserializer", classOf[StringDeserializer].getName)
    props.setProperty("auto.offset.reset", "latest")
    import org.apache.flink.streaming.api.scala._
    environment.addSource(new FlinkKafkaConsumer[(String, String)](
      // 使用匿名内部类的形式
      topic, new KafkaDeserializationSchema[(String, String)] {
        override def isEndOfStream(nextElement: (String, String)) = {
          false
        }

        override def deserialize(record: ConsumerRecord[Array[Byte], Array[Byte]]) = {
          if (record!=null){
            var key="null"
            var value="null"
            if (record.key()!=null){
              key=new String(record.key(),"utf-8")
            }
            if (record.value()!=null){
              value=new String(record.value(),"utf-8")
            }
            (key,value)
          }else{
            ("null","null")
          }
        }

        override def getProducedType = {
          createTuple2TypeInformation(createTypeInformation[String],createTypeInformation[String])
        }
      },props).setCommitOffsetsOnCheckpoints(true))
      .filter(_._1.contains("hello"))
      .print()
    environment.execute(this.getClass.getName)
    /**
      *  可以设置指定时间的偏移量  下面的几种方式
      *
      * setStartFromTimestamp(68768758588L)
      *
      * setStartFromEarliest()
      *
      * setStartFromGroupOffsets()
      *
      * setCommitOffsetsOnCheckpoints(True)
      */

  }
}
