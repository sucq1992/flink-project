package com.flink.source

import java.util.Properties

import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 17:24:13
  */
object KafkaSimpleSource {
  def main(args: Array[String]): Unit = {
    lazy val  topic :String ="my_tp"

    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    // 配置kafka属性
    val props = new Properties()
    props.setProperty("bootstrap.servers","node1:9092,node2:9092,node3 3:9092")
    props.setProperty("group.id","fink01")
    props.setProperty("key.deserializer",classOf[StringDeserializer].getName)
    props.setProperty("value.deserializer",classOf[StringDeserializer].getName)
    props.setProperty("auto.offset.reset","latest")

    //  添加数据源
    environment.addSource(new FlinkKafkaConsumer[String](
      topic,new SimpleStringSchema(),props
    )).filter(_.contains("hive"))
      .print()
    environment.execute(this.getClass.getName)
  }
}
