package com.flink.source

import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

import scala.util.Random

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 17:27:06
  */
object CustomerSource {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.addSource(new mySource)
        .filter(_.bage>20)
        .print()

    environment.execute(this.getClass.getName)
  }
}

class mySource extends SourceFunction[Bean]{
  var flag = true
  val random = new Random()
  var names =Array("tom","mary","jack","mike")

  override def run(ctx: SourceFunction.SourceContext[Bean]): Unit = {
      while (flag){
        1.to(50).map(i=>{
          val bid= random.nextLong().toString
          val bname=names(random.nextInt(names.length))
          val bage=random.nextInt(100)
          new Bean(bid,bname,bage)
        }).foreach(ctx.collect(_))
      Thread.sleep(3000)
      }
  }

  override def cancel(): Unit = {
    flag=false
  }
}


case class Bean(bid:String,bname:String,bage:Int)