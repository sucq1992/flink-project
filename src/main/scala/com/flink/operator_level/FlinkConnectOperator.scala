package com.flink.operator_level

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 14:07:00
  */
object FlinkConnectOperator {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    val stream_1: DataStream[(String, Int)] = environment.readTextFile("/Users/run/Downloads/workspaceIDEA/flink-project/data/word.txt")
      .flatMap(_.split(" "))
      .map((_, 1))
      .keyBy(0)
      .sum(1)


    val stream_2: DataStream[(String, Int)] = environment.socketTextStream("localhost", 9876)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .keyBy(0)
      .sum(1)
      val midData: ConnectedStreams[(String, Int), (String, Int)] = stream_1.connect(stream_2)
      // 对连接后的数据，需要处理 分开按照两个函数处理
    val res: DataStream[(String, Int)] = midData.map(
      t1 => {
        (t1._1, t1._2)
      },
      t2 => {
        (t2._1, t2._2)
      }
    )
    res.print()
    environment.execute(this.getClass.getName)
  }
}
