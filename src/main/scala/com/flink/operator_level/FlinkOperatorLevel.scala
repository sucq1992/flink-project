package com.flink.operator_level

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-04 23:11:51
  */
object FlinkOperatorLevel {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)   // 环境层面
    import org.apache.flink.streaming.api.scala._
    environment.readTextFile("data/word.txt")
        .flatMap(_.split(" ")).setParallelism(9) // 算子层面
        .filter(_.nonEmpty).setParallelism(2)
        .map((_,1))
        .keyBy(0)
        .sum(1)
        .print()
    environment.execute(this.getClass.getName)
  }
}
