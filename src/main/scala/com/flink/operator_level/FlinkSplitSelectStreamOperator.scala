package com.flink.operator_level

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 14:07:27
  */
object FlinkSplitSelectStreamOperator {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    val stream: DataStream[(String, Int)] = environment.readTextFile("/Users/run/Downloads/workspaceIDEA/flink-project/data/word.txt")
      .flatMap(_.split(" "))
      .map((_, 1))
      .keyBy(0)
      .sum(1)

    stream.split(data=>{
      if (data._2%2==0){
        List("odd")
      }else{
        List("even")
      }
    }) .select("odd").print()
    environment.execute(this.getClass.getName)

    /**
      * 对于选择流和切分流
      * 可使用这个来完成
      */
  }
}
