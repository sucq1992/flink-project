package com.flink.operator_level

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 14:06:27
  */
object FlinkUnionOperator {
  def main(args: Array[String]): Unit = {
  val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    val stream_1: DataStream[(String, Int)] = environment.readTextFile("/Users/run/Downloads/workspaceIDEA/flink-project/data/word.txt")
      .flatMap(_.split(" "))
      .map((_, 1))
      .keyBy(0)
      .sum(1)

    val stream_2: DataStream[(String, Int)] = environment.socketTextStream("localhost", 9876)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .keyBy(0)
      .sum(1)
    val result: DataStream[(String, Int)] = stream_1.union(stream_2)
    result.print()
    environment.execute(this.getClass.getName)
  }
}
