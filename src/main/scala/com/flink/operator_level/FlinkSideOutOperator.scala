package com.flink.operator_level

import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.streaming.api.scala.{OutputTag, StreamExecutionEnvironment}
import org.apache.flink.util.Collector

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 14:08:28
  */
// 隐式转换必须在前设置
import org.apache.flink.streaming.api.scala._

object FlinkSideOutOperator {
  // 对于侧输出流来说，需要先定义一个标签
  val fail_tag = new OutputTag[StationLog]("not_success")

  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    val stream: DataStream[StationLog] = environment.addSource(new myStationLogSource)
      .process(new SideOutputStreamFunction(fail_tag))

    stream.print("success")

    val fail_stream: DataStream[StationLog] = stream.getSideOutput(fail_tag)

    fail_stream.print("fail")
    environment.execute(this.getClass.getName)
  }
}

/**
  *  ProcessFunction 的处理
  */
class SideOutputStreamFunction(tag: OutputTag[StationLog]) extends ProcessFunction[StationLog,StationLog]{
  override def processElement(value: StationLog, ctx: ProcessFunction[StationLog, StationLog]#Context, out: Collector[StationLog]): Unit = {
    if (!value.callType.equals("failed")){
      out.collect(value)  // 输出到主流
    }else{
      ctx.output(tag,value)   //输出到侧流
    }
  }
}