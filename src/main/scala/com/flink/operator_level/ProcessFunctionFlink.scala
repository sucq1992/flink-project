package com.flink.operator_level

import java.util.concurrent.TimeUnit

import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.util.Collector

import scala.util.Random

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 16:09:42
  */

/**
  * 监控每一个手机号，如果在5秒内呼叫它的通话都是失败的，
  * 发出警告信息 * 在5秒中内只要有一个呼叫不是fail则不用警告
  */
object ProcessFunctionFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.addSource(new myStationLogSource)
      .keyBy(_.callOut)
      .process(new MonitorCallFail)
      .print()

    environment.execute(this.getClass.getName)
  }
}

/**
  * source function
  */
class myStationLogSource extends SourceFunction[StationLog] {

  var flag = true
  private val random = new Random()
  val call_types = Array("busy", "success", "failed", "barrier")

  override def run(ctx: SourceFunction.SourceContext[StationLog]): Unit = {
    while (flag) {
      // 生成100个
      1.to(100).map(i => {
        // 生成数据
        val sid = i + "" //基站id
        val call_out = "1869999%04d".format(random.nextInt(10000)) // 主叫号码
        val call_in = "1878888%04d".format(random.nextInt(10000)) //被叫
        val call_type = call_types(random.nextInt(call_types.length)) //通话类型
        val call_time = System.currentTimeMillis()
        val duration = random.nextInt(1000)
        new StationLog(sid, call_in, call_out, call_type, call_time, duration)
      }).foreach(ctx.collect(_)) //发送数据到流

      TimeUnit.SECONDS.sleep(3)
    }
  }

  override def cancel(): Unit = {
    flag = false
  }
}

/**
  * 监控手机呼叫失败的手机号
  */
class MonitorCallFail extends KeyedProcessFunction[String, StationLog, String] {
  //使用一个状态记录时间
  lazy val status: ValueState[Long] = getRuntimeContext.getState(new ValueStateDescriptor[Long]("time", classOf[Long]))

  /**
    * 处理逻辑
    *
    * @param value
    * @param ctx
    * @param out
    */
  override def processElement(value: StationLog, ctx: KeyedProcessFunction[String, StationLog, String]#Context, out: Collector[String]): Unit = {
    // 首先从状态中获取时间
    val status_time: Long = status.value()
    // 如果第一次是失败的。并且时间是0
    if (value.callType.equals("failed") && status_time == 0) {
      //此时获取当前时间  从上下文获取
      val now_time: Long = ctx.timerService().currentProcessingTime()
      // 设置5s之后触发
      val new_time: Long = now_time + 5000L
      // 需注册定时器
      ctx.timerService().registerProcessingTimeTimer(new_time)
      // 更新时间
      status.update(new_time)
    }
    // 如果这段时间有呼叫成功的，那么取消触发器
    if (!value.callType.equals("failed") && status_time != 0) {
      // 通过上下文来删除定时器
      ctx.timerService().deleteProcessingTimeTimer(status_time)
      status.clear() // 清除时间
    }

  }

  /**
    * 时间到了，执行触发器,发出告警
    *
    * @param timestamp
    * @param ctx
    * @param out
    */
  override def onTimer(timestamp: Long, ctx: KeyedProcessFunction[String, StationLog, String]#OnTimerContext, out: Collector[String]): Unit = {
    // 返回的信息
    val str: String = " 触发时间: " + timestamp + " 手机号： " + ctx.getCurrentKey
    // 收集信息
    out.collect(str)
    // 清空时间
    status.clear()
  }
}


/**
  * 基站日志样例类
  *
  * @param sid
  * @param callIn
  * @param callOut
  * @param callType
  * @param callTime
  * @param duration
  */
case class StationLog(sid: String, callIn: String, callOut: String, callType: String, callTime: Long, duration: Long)
