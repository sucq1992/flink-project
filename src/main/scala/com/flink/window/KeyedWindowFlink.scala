package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 13:56:00
  */
object KeyedWindowFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(3)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",3455)
      .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
        .window(EventTimeSessionWindows.withGap(Time.seconds(50)))
        .sum(1)
        .print()

    environment.execute(this.getClass.getName)
  }
}
