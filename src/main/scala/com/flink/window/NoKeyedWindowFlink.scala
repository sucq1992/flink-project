package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 13:56:42
  */
object NoKeyedWindowFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(3)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",3455)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_,1))
      .windowAll(SlidingEventTimeWindows.of(Time.seconds(20),Time.seconds(30)))
        
    environment.execute(this.getClass.getName)
  }
}
