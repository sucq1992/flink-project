package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 13:30:41
  */
object SessionWindowFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8755)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
      // 计算5分钟的会话的浏览量
        .window(EventTimeSessionWindows.withGap(Time.minutes(5)))
        .sum(1)
        .print()
    environment.execute(this.getClass.getName)
  }
}
