package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 13:25:35
  */
object SlidingEventTimeWindowsFlink {
  def main(args: Array[String]): Unit = {
    /**
      * 滑动窗口
      * 每10秒钟计算前1分钟的页面浏览量
      */
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(2)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8898)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .keyBy(0)
      .window(SlidingEventTimeWindows.of(Time.minutes(1),
        Time.seconds(10)))
      .sum(1)
      .print()
    environment.execute(this.getClass.getName)

  }
}
