package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 11:31:41
  */
object TumblingEventTimeWindowsFlink {
  def main(args: Array[String]): Unit = {
  val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(2)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8898)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .keyBy(0)
      // 每分钟页面浏览量
      // 滚动窗口
        .window(TumblingEventTimeWindows.of(Time.seconds(60)))
        .sum(1)
        .print()
    environment.execute(this.getClass.getName)
  }
}
