package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 14:24:17
  */
object ProcessWindowFunctionFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(3)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",3455)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_,1))
      .keyBy(0)
      .window(TumblingEventTimeWindows.of(Time.minutes(5)))
      .sum(1)
      .print()
    environment.execute(this.getClass.getName)

//      .process(new MyProcessWindowFunction)
  }
}
//   MyProcessWindowFunction 导包使用下面的短的包 窗口函数的计算
class  MyProcessWindowFunction extends ProcessWindowFunction[(String,Long),String,String,TimeWindow]{
  override def process(key: String,
                       context: Context,
                       elements: Iterable[(String, Long)],
                       out: Collector[String]): Unit = {
    var count = 0l
    for (ele<-elements){
      count+=1
    }
    out.collect(s"${context.window}:count:${count}")
  }
}