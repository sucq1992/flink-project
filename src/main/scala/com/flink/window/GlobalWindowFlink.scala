package com.flink.window

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.assigners.GlobalWindows

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 14:12:21
  */
object GlobalWindowFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(2)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8898)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .keyBy(0)
      .window(GlobalWindows.create())  // todo
      .sum(1)
      .print()
    environment.execute(this.getClass.getName)
  }
}
