package com.flink.operator

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 13:47:48
  */
object AggregationsFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8888)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
        .minBy(2)
        .print()
    environment.execute(this.getClass.getName)
  }
}
