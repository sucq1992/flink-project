package com.flink.operator

import org.apache.flink.streaming.api.CheckpointingMode
import org.apache.flink.streaming.api.environment.CheckpointConfig.ExternalizedCheckpointCleanup
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 22:36:34
  */
object FlinkCkeckPoint {
  def main(args: Array[String]): Unit = {
    /**
      * 默认情况下 Flink 不开启检查点的，用户需要在程序中通过调用方法配置和开启检查点
      * 开启检查点，并设置检查点的模式，按照精准一次
      *
      */
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    environment.enableCheckpointing(1000,
      //      CheckpointingMode.EXACTLY_ONCE)  // 设置开启检查点 和时间语义
      CheckpointingMode.AT_LEAST_ONCE) // 如果对数据要求不太高，可以这样设置

    /** 默认情况是 EXACTLY_ONCE
      * def enableCheckpointing(interval : Long) : StreamExecutionEnvironment = {
      * enableCheckpointing(interval, CheckpointingMode.EXACTLY_ONCE)
      * }
      */
    //对于flink check point超时时间,每次check_point的执行时间上线一旦超过这个阈值会暂停check_point
    // 默认是超时时间10分钟  val MINIMAL_CHECKPOINT_TIME: Long = 10
    environment.getCheckpointConfig.setCheckpointTimeout(50000)
    // 检查点之间的最小间隔
    environment.getCheckpointConfig.setMinPauseBetweenCheckpoints(600)

    // 最大并行检查点的个数
    environment.getCheckpointConfig.setMaxConcurrentCheckpoints(4)

    // 是否删除检查点的数据
    environment.getCheckpointConfig
      .enableExternalizedCheckpoints(
        ExternalizedCheckpointCleanup.DELETE_ON_CANCELLATION) // 删除检查点数据
    environment.getCheckpointConfig
      .enableExternalizedCheckpoints(
        ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION) // 保留检查点数据
    // 容忍的检查点失败数
    environment.getCheckpointConfig
      .setTolerableCheckpointFailureNumber(2)

    environment.execute(this.getClass.getName)
  }
}
