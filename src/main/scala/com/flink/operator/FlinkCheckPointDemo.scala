package com.flink.operator

import org.apache.flink.contrib.streaming.state.RocksDBStateBackend
import org.apache.flink.streaming.api.CheckpointingMode
import org.apache.flink.streaming.api.environment.CheckpointConfig
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 09:31:31
  */
object FlinkCheckPointDemo {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.api.scala._
    environment.enableCheckpointing(5000) //设置开启检查点
    environment.setStateBackend(new RocksDBStateBackend("hdfs://xxx")) // 设置状态后端
    environment.getCheckpointConfig
      .setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE) // 设置检查点模式

    environment.getCheckpointConfig.setCheckpointTimeout(5000) // 设置检查点超时时间

    environment.getCheckpointConfig // 设置是否删除检查点
      .enableExternalizedCheckpoints(
      CheckpointConfig.
        ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION)

    // 设置容忍的检查点失败次数
    environment.getCheckpointConfig.setTolerableCheckpointFailureNumber(2)
    environment.socketTextStream("localhost", 7676)
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1)).keyBy(0)
      .sum(1)
      .print()
    environment.execute(this.getClass.getName)


  }
}
