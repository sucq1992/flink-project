package com.flink.operator

import org.apache.flink.api.common.restartstrategy.RestartStrategies
import org.apache.flink.api.common.time.Time
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 10:39:53
  */
/**
  * flink 重启策略
  */
object FlinkRestartStrategy {
  def main(args: Array[String]): Unit = {
    /**
      * 重启策略需要配合 check_point todo
      *
      * 设置重启策略  RestartStrategies 类下的重启策略
      *     environment.setRestartStrategy(RestartStrategies.noRestart())
      */
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._

    environment.enableCheckpointing(5000) // 开启检查点
    // 设置重启策略
    environment.setRestartStrategy(RestartStrategies.noRestart())  // 无重启策略

    /**
      * failureRate Maximum number of restarts in given interval {@code failureInterval} before failing a job
      * failureInterval Time interval for failures
      * delayInterval Delay in-between restart attempts
      */
    // 故障率重启策略,注意参数
    environment.setRestartStrategy(RestartStrategies
      .failureRateRestart(2,
        Time.minutes(1),
        Time.seconds(50)))

    // fallBackRestart
    environment.setRestartStrategy(RestartStrategies.fallBackRestart())

    //fixedDelayRestart   固定延迟重启策略
    environment.setRestartStrategy(RestartStrategies
      .fixedDelayRestart(4,Time.minutes(15)))

      environment.socketTextStream("localhost",7653)
          .flatMap(_.split(" "))
          .filter(_.nonEmpty)
          .map((_,1))
          .keyBy(0)
          .sum(1).print()


    environment.execute(this.getClass.getName)
  }
}
