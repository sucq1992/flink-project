package com.flink.operator

import org.apache.flink.api.common.functions.MapFunction
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 14:54:05
  */
object FlinkMapFunction {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    environment.readTextFile("/Users/run/Downloads/workspaceIDEA/flink-project/data/word.txt")
      .flatMap(_.split(" "))
      .map(new myMapFun)
      .keyBy(0)
      .sum(1)
      .print()
    environment.execute(this.getClass.getName)
  }
}

/**
  * 形如 mapFunction
  * flatFunction
  * 等都可以按照这个来完成对应的方法
  *
  *
  */
class myMapFun extends MapFunction[String, (String, Int)] {
  override def map(value: String): (String, Int) = {
    (value, 1)
  }
}
