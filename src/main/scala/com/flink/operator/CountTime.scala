package com.flink.operator

import com.flink.operator_level.myStationLogSource
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 22:18:56
  */
object CountTime {
  def main(args: Array[String]): Unit = {
    /**
      * 每隔5秒统计每个基站的日志数量
      */
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.addSource(new myStationLogSource)
        .map(data=>{
          (data.sid,1)
        })
        .keyBy(1)
        .timeWindow(Time.seconds(5))
//        .process()

        .sum(1)
        .print()
    //直接使用这个时间timeWindow 来计算每隔一定时间的数据

    environment.execute(this.getClass.getName)
  }
}

/** 注意ProcessWindowFunction 所在的包
  * 报错，类型不一致
  *
  */
class processFunByTime extends ProcessWindowFunction[(String,Int),(String,Int),String,TimeWindow]{

  override def process(key: String,
                       context: Context,
                       elements: Iterable[(String, Int)],
                       out: Collector[(String, Int)]): Unit = {
  out.collect((key,elements.size))
}
}
