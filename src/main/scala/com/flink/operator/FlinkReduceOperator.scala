package com.flink.operator

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 13:42:59
  */
object FlinkReduceOperator {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",8675)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
        .reduce((t1,t2)=>{
          (t1._1,t1._2+t2._2)
        })
        .print()
    environment.execute(this.getClass.getName)
  }
}
