package com.flink.operator

import org.apache.flink.contrib.streaming.state.RocksDBStateBackend
import org.apache.flink.runtime.state.filesystem.FsStateBackend
import org.apache.flink.runtime.state.memory.MemoryStateBackend
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-06 07:29:05
  */
object StateBackendFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._
    environment.socketTextStream("localhost",9876)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
        .sum(1)
    // 设置状态后端  方法过时
    // Use [[StreamExecutionEnvironment.setStateBackend(StateBackend)]] instead.
    environment.setStateBackend(new MemoryStateBackend(1024*1024*1024))
    environment.setStateBackend(new FsStateBackend("hdfs://"))
    environment.setStateBackend(new RocksDBStateBackend("hdfs://"))

    environment.execute(this.getClass.getName)
  }
}
