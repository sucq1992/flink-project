package com.flink.sink

import com.flink.source.{Bean, mySource}
import org.apache.flink.api.common.serialization.SimpleStringEncoder
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 09:28:43
  */
object HDFSSinkOfFlink2 {
  def main(args: Array[String]): Unit = {
    import org.apache.flink.streaming.api.scala._
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 获取并行度
    val parallelism: Int = environment.getParallelism
    //设置文件滚动策略
    val policy: DefaultRollingPolicy[Bean, String] = DefaultRollingPolicy.builder()
      .withInactivityInterval(1000) //  设置不活动分桶时间 10s
      .withRolloverInterval(6000) //设置每隔60s生成一个文件
      .withMaxPartSize(1024 * 1024 * 1024) // 设置文件滚动的最大大小
      .build[Bean, String]()

    val sink: StreamingFileSink[Bean] = StreamingFileSink.forRowFormat[Bean](
      new Path("hdfs://"),
      // 字符编码默认是 UTF-8
      new SimpleStringEncoder[Bean]("utf-8"))
      .withRollingPolicy(policy)
      .withBucketCheckInterval(3600) // 检查分桶的时间间隔
      .build()

    // 添加数据源，根据数据源来写入文件系统
    environment.addSource(new mySource)
      .addSink(sink)


    environment.execute(this.getClass.getName)

  }
}
