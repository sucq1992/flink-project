package com.flink.sink

import java.sql.{Connection, DriverManager, PreparedStatement}

import com.flink.source.{Bean, mySource}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 11:32:44
  */
object CustomerSinkFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
    environment.setParallelism(4)
    environment.addSource(new mySource)
        .addSink(new myCustomerSink)
    environment.execute(this.getClass.getName)
  }
}

/**
  * 自定义sink
  */
class myCustomerSink extends RichSinkFunction[Bean]{
  var conn:Connection =_
  var pst :PreparedStatement =_
  val username= "root"
  val password="scq110801502"
  val driverClass="com.mysql.jdbc.driver"
  val url="jdbc:mysql://localhost:3306/mydb?useUnicode=true&characterEncoding=utf-8&relaxAutoCommit=true" +
    "&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true"

  override def open(parameters: Configuration): Unit = {
    conn=DriverManager.getConnection(url,username,password)
    pst=conn.prepareStatement("insert into t_user values (?,?,?)")
  }


  override def invoke(value: Bean, context: SinkFunction.Context[_]): Unit = {
    pst.setString(1,value.bid)
    pst.setString(2,value.bname)
    pst.setInt(3,value.bage)
  }

  override def close(): Unit = {
    conn.close()
    pst.close()
  }
}