package com.flink.sink

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.redis.RedisSink
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig
import org.apache.flink.streaming.connectors.redis.common.mapper.{RedisCommand, RedisCommandDescription, RedisMapper}

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 09:51:34
  */
object RedisSinkOfFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(4)
    import org.apache.flink.streaming.api.scala._

    val config: FlinkJedisPoolConfig = new FlinkJedisPoolConfig.Builder()
      .setDatabase(0)
      .setHost("localhost")
      .setPort(6379)
      .build()


    environment.socketTextStream("localhost",9892)
        .flatMap(_.split(" "))
        .filter(_.nonEmpty)
        .map((_,1))
        .keyBy(0)
        .sum(1)
        .addSink(new RedisSink[(String,Int)](config,new RedisMapper[(String,Int)]() {
          override def getCommandDescription = {
            // t_key 相当于是表名
            new RedisCommandDescription(RedisCommand.HSET,"t_key")
          }

          override def getKeyFromData(data: (String, Int)) = {
              data._1
          }

          override def getValueFromData(data: (String, Int)) = {
            data._2+""
          }
        }))

    environment.execute(this.getClass.getName)
  }
}

/**
  * 通过单独的类完成:  写入redis
  * 或者在这里通过单独类来完成
  * RedisCommand的命令
  */
class myRedisMapper extends RedisMapper[(String,Int)]{

  override def getCommandDescription: RedisCommandDescription = {
    new RedisCommandDescription(RedisCommand.HSET,"t_my_table_key")
  }

  override def getKeyFromData(data: (String, Int)): String = {
    data._1
  }

  override def getValueFromData(data: (String, Int)): String = {
    data._2+""
  }
}