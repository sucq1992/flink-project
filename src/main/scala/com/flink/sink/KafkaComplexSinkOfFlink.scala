package com.flink.sink

import java.lang
import java.util.Properties

import org.apache.flink.api.common.functions.Partitioner
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaProducer, KafkaSerializationSchema}
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 09:52:06
  */
object KafkaComplexSinkOfFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    environment.setParallelism(6)
    import org.apache.flink.streaming.api.scala._
    val props = new Properties()
    props.setProperty("bootstrap.servers","node1:9092,node2:9092,node3 3:9092")
    props.setProperty("group.id","fink01")
    props.setProperty("key.deserializer",classOf[StringSerializer].getName)
    props.setProperty("value.deserializer",classOf[StringSerializer].getName)
    environment.socketTextStream("localhost",1806)
      .flatMap(_.split(""))
      .filter(_.nonEmpty)
      .map((_,1))
      .keyBy(0)
      .sum(1)
      .addSink(new FlinkKafkaProducer[(String, Int)](
        "topic",
        new myKafkaComplex,  // 单独的类完成 或者是使用匿名内部类
        props,
        FlinkKafkaProducer.Semantic.EXACTLY_ONCE))   //指定时间语义
    environment.execute(this.getClass.getName)
  }
}

/**
  * kafka 整合 flink 键值对形式
  */
class myKafkaComplex extends KafkaSerializationSchema[(String,Int)]{
  override def serialize(element: (String, Int), timestamp: lang.Long): ProducerRecord[Array[Byte], Array[Byte]] = {
    new ProducerRecord("topic",element._1.getBytes,(element._2+"").getBytes)
  }
}


/**
  * 自定义分区
  */
class myPartitioner extends  Partitioner[String]{
  override def partition(key: String, numPartitions: Int): Int = {
      if (key.hashCode%2==0){
        0
      }else{
        1
      }
  }
}