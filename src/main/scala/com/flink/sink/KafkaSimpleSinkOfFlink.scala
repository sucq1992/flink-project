package com.flink.sink

import java.util.Properties

import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer
import org.apache.kafka.common.serialization.StringSerializer

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 09:51:50
  */
object KafkaSimpleSinkOfFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 设置并行度
    environment.setParallelism(6)
    import org.apache.flink.streaming.api.scala._
    val props = new Properties()
    props.setProperty("bootstrap.servers","node1:9092,node2:9092,node3 3:9092")
    props.setProperty("group.id","fink01")
    props.setProperty("key.deserializer",classOf[StringSerializer].getName)
    props.setProperty("value.deserializer",classOf[StringSerializer].getName)
   environment.socketTextStream("localhost",9898)
       .flatMap(_.split(" "))
       .filter(_.nonEmpty)
       .addSink(new FlinkKafkaProducer[String](
         "topic",new SimpleStringSchema(),props))
    environment.execute(this.getClass.getName)
  }
}
