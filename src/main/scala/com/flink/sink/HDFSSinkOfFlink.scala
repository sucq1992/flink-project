package com.flink.sink

import org.apache.flink.core.fs.FileSystem
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-05 09:24:46
  */
object HDFSSinkOfFlink {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    import org.apache.flink.streaming.api.scala._
      environment.readTextFile("data/word.txt")
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_,1))
      .keyBy(0)
      .sum(1)
      .writeAsText("xxxx",FileSystem.WriteMode.OVERWRITE)
//      .writeAsCsv("xxxx",FileSystem.WriteMode.OVERWRITE)
    environment.execute(this.getClass.getName)

  }
}
