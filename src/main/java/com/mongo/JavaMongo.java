package com.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
public class JavaMongo {
  public static void main(String[] args) {
      /**
       * java 连接mongoDB
       *
       * Exception in thread "main" java.lang.NoSuchMethodError:
       * com.mongodb.internal.operation.SyncOperations.
       */
    MongoClient client = new MongoClient("localhost:27017");
    MongoDatabase database = client.getDatabase("my_mongo");
    MongoCollection<Document> mongo = database.getCollection("account");
    long countDocuments = mongo.countDocuments();
    System.out.println("countDocuments = " + countDocuments);
    client.close();

  }
}
