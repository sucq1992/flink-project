package com.flink.train

import scala.util.control.Breaks

/**
  * @program: flink-project
  * @author run
  * @Version run
  * @Modified run
  * @date 2021-08-09 10:24:59
  */
object ScalaBreak {
  def main(args: Array[String]): Unit = {
    val loop = new Breaks()

    loop.breakable({
      for (i <- 1 until (10)) {
        println(i)
        if (i == 5) {
          loop.break()
        }
      }
    })
  }
}
